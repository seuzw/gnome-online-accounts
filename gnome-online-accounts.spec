%global gettext_version 0.19.8
%global glib2_version 2.52
%global gtk3_version 3.19.12
%global libsoup_version 2.42
%global webkit2gtk3_version 2.26.0

Name:           gnome-online-accounts
Version:        3.44.0
Release:        1
Summary:        Single sign-on framework for GNOME
License:        LGPLv2+
URL:            https://wiki.gnome.org/Projects/GnomeOnlineAccounts
Source0:        https://download.gnome.org/sources/gnome-online-accounts/3.44/%{name}-%{version}.tar.xz
Patch0:         0001-google-Remove-Photos-support.patch

BuildRequires:  make vala gtk-doc krb5-devel
BuildRequires:  pkgconfig(gcr-3)
BuildRequires:  pkgconfig(gio-2.0) >= %{glib2_version}
BuildRequires:  pkgconfig(glib-2.0) >= %{glib2_version}
BuildRequires:  pkgconfig(gobject-2.0) >= %{glib2_version}
BuildRequires:  pkgconfig(gtk+-3.0) >= %{gtk3_version}
BuildRequires:  pkgconfig(gobject-introspection-1.0)
BuildRequires:  gettext >= %{gettext_version}
BuildRequires:  pkgconfig(webkit2gtk-4.0) >= %{webkit2gtk3_version}
BuildRequires:  pkgconfig(json-glib-1.0)
BuildRequires:  pkgconfig(libsecret-1) >= 0.7
BuildRequires:  pkgconfig(libsoup-2.4) >= %{libsoup_version}
BuildRequires:  pkgconfig(rest-0.7)
BuildRequires:  pkgconfig(libxml-2.0)

Requires:       glib2%{?_isa} >= %{glib2_version}
Requires:       gtk3%{?_isa} >= %{gtk3_version}
Requires:       libsoup%{?_isa} >= %{libsoup_version}
Requires:       webkit2gtk3%{?_isa} >= %{webkit2gtk3_version}

%description
GNOME Online Accounts Single sign-on framework for GNOME. It aims to provide
a way for users to setup online accounts to be used by the core system and
core applications only. Calendar entries show up in GNOME Shell, e-mail in
Evolution, online storages are exposed as GVolumes, and so on.

%package devel
Summary:        Libraries and header files
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
This package includes libraries and header files for
developing with gnome-online-accounts.

%package help
Summary: Man pages for gnome-online-accounts

%description help
Man pages for gnome-online-accounts.

%prep
%autosetup -n %{name}-%{version} -p1
#%setup -q
#%if 0%{?rhel} >= 9
#%patch0 -p1
#%endif

%build
%configure --disable-lastfm --disable-media-server --disable-pocket \
  --disable-silent-rules --disable-static --disable-todoist --enable-documentation \
  --enable-facebook --enable-foursquare --enable-exchange --enable-flickr \
  --enable-google --enable-gtk-doc --enable-imap-smtp --enable-kerberos \
  --enable-owncloud --enable-windows-live

%make_build

%install
%make_install

%delete_la_and_a

%find_lang %{name}

%ldconfig_post
%ldconfig_postun

%files -f %{name}.lang
%license COPYING
%doc COPYING
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/Goa-1.0.typelib
%{_libdir}/libgoa-1.0.so.0
%{_libdir}/libgoa-1.0.so.0.0.0
%{_libdir}/libgoa-backend-1.0.so.1
%{_libdir}/libgoa-backend-1.0.so.1.0.0
%dir %{_libdir}/goa-1.0
%dir %{_libdir}/goa-1.0/web-extensions
%{_libdir}/goa-1.0/web-extensions/libgoawebextension.so
%{_prefix}/libexec/goa-daemon
%{_prefix}/libexec/goa-identity-service
%{_datadir}/dbus-1/services/org.gnome.OnlineAccounts.service
%{_datadir}/dbus-1/services/org.gnome.Identity.service
%{_datadir}/icons/hicolor/*/apps/goa-*.svg
%{_datadir}/glib-2.0/schemas/org.gnome.online-accounts.gschema.xml

%files devel
%{_includedir}/goa-1.0/
%{_libdir}/libgoa-1.0.so
%{_libdir}/libgoa-backend-1.0.so
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/Goa-1.0.gir
%{_libdir}/pkgconfig/goa-1.0.pc
%{_libdir}/pkgconfig/goa-backend-1.0.pc
%dir %{_datadir}/gtk-doc
%dir %{_datadir}/gtk-doc/html
%{_datadir}/gtk-doc/html/goa/
%{_libdir}/goa-1.0/include
%{_datadir}/vala/

%files help
%{_datadir}/man/man8/goa-daemon.8*

%changelog
* Mon Jun 6 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 3.44.0-1
- update to 3.44.0

* Thu May 19 2022 loong_C <loong_c@yeah.net> - 3.43.1-3
- update gnome-online-accounts.spec

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 3.43.1-2
- update to 3.43.1-2

* Tue Jul 20 2021 liuyumeng <liuyumeng5@huawei.com> - 3.38.2-2
- delete gdb in buildrequires

* Mon May 24 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.2-1
- Upgrade to 3.38.2
- Update Version

* Fri Jan 29 2021 yanglu <yanglu60@huawei.com> - 3.38.0-1
- update to 3.38.0

* Fri Mar 20 2020 songnannan <songnannan2@huawei.com> - 3.30.0-5
- add gdb in buildrequires

* Wed Sep 18 2019 Zaiwang Li <lizaiwang1@huawei.com> - 3.30.0-4
- Init Package
